
const root = document.getElementById("root");
const form1 = document.createElement("form");
form1.name = "formMeteo"
form1.innerHTML = `<form name="formMeteo" id="form">
<h2>Inserisci Latitude e Longitudine del posto che vuoi cercare</h2>
<br>
<div class="row">
  <div class="col">
    <input type="text" class="form-control" name="latitude" placeholder="Latitude" required>
  </div>
  <div class="col">
    <input type="text" class="form-control" name="longitude" placeholder="Longitude" required>
  </div>
</div>
<br>
<input type="submit" class ="btn btn-success" id="forButton" value="Cerca">
</form>`

const header = document.createElement("header");
header.innerHTML = `<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
<a class="navbar-brand" href="#">MeteoLog</a>
<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
  <span class="navbar-toggler-icon"></span>
</button>

<div class="collapse navbar-collapse" id="navbarSupportedContent">
  <ul class="navbar-nav mr-auto">
    <li class="nav-item active">
      <a class="nav-link" href="#">Home <span class="sr-only"></span></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="#">Servizio</a>
    </li>

  </ul>
</div>
</nav>`

const section1 = document.createElement("section");
section1.className = "section1";


const table = document.createElement("table");
table.className = "table table-striped";
table.style = "margin-top: 100px, background-color: white"

const thead = document.createElement("thead");
table.appendChild(thead);

const th0 = document.createElement("th");
th0.textContent = "Previsioni Cercate";
th0.scope = "col";
thead.appendChild(th0);

const th1 = document.createElement("th");
th1.textContent = "Orario";
th1.scope = "col";
thead.appendChild(th1);

const th2 = document.createElement("th");
th2.textContent = "Latitudine";
th2.scope = "col";
thead.appendChild(th2);

const th3 = document.createElement("th");
th3.textContent = "Longitudine";
th3.scope = "col";
thead.appendChild(th3);

const th4 = document.createElement("th");
th4.textContent = "Temperatura";
th4.scope = "col";
thead.appendChild(th4);

const th5 = document.createElement("th");
th5.textContent = "Vento";
th5.scope = "col";
thead.appendChild(th5);

const tbody = document.createElement("tbody");
table.appendChild(tbody);



const formSpecial = document.forms.formMeteo;

root.appendChild(header);
root.appendChild(form1);
form1.appendChild(section1);
root.appendChild(table);

const latitudeField = formMeteo.latitude;
const longitudeField = formMeteo.longitude;

let div1;

function createCard(res) {
    div1 = document.createElement("div");
    div1.id = "div1";
    div1.className = "card";
    div1.style = "width: 20rem;"

    const div2 = document.createElement("div");
    div2.className = "card-body";
    div1.appendChild(div2);

    const title = document.createElement("h5");
    title.textContent = "Previsione";
    title.className = "card-title";
    div2.appendChild(title);

    div2.appendChild(document.createElement("br"));

    const p1 = document.createElement("p");
    p1.className = "card-text cardPatter";
    p1.textContent = `Ora misurazione: ${res.current.time}`;
    div2.appendChild(p1);

    const p2 = document.createElement("p");
    p2.className = "card-text cardPatter";
    p2.textContent = `Latitudine: ${res.latitude} ${res.current_units.temperature_2m}°`;
    div2.appendChild(p2);

    const p3 = document.createElement("p");
    p3.className = "card-text cardPatter";
    p3.textContent = `Longitudine: ${res.longitude}°`;
    div2.appendChild(p3);

    const p4 = document.createElement("p");
    p4.className = "card-text cardPatter";
    p4.textContent = `Temperatura : ${res.current.temperature_2m}${res.current_units.temperature_2m}`;
    div2.appendChild(p4);

    const p5 = document.createElement("p");
    p5.className = "card-text cardPatter";
    p5.textContent = `Vento: ${res.current.wind_speed_10m}${res.current_units.wind_speed_10m}`;
    div2.appendChild(p5);

    section1.appendChild(div1);
}

let index = 1;

function createRow(res) {

    const tr = document.createElement("tr");
    tbody.appendChild(tr);

    const thIndex = document.createElement("th");
    thIndex.scope = "row";
    thIndex.textContent = index++;
    tr.appendChild(thIndex);

    const td1 = document.createElement("td");
    td1.textContent = `${res.current.time}`;
    tr.appendChild(td1);

    const td2 = document.createElement("td");
    td2.textContent = `${res.latitude} ${res.current_units.temperature_2m}`;
    tr.appendChild(td2);

    const td3 = document.createElement("td");
    td3.textContent = `${res.longitude} ${res.current_units.temperature_2m}`;
    tr.appendChild(td3);

    const td4 = document.createElement("td");
    td4.textContent = `${res.current.temperature_2m}${res.current_units.temperature_2m}`;
    tr.appendChild(td4);

    const td5 = document.createElement("td");
    td5.textContent = `${res.current.wind_speed_10m}${res.current_units.wind_speed_10m}`;
    tr.appendChild(td5);
}

document.getElementById("forButton").addEventListener('click', async (e) => {
    e.preventDefault();
    const response = await fetch(`https://api.open-meteo.com/v1/forecast?latitude=${latitudeField.value}&longitude=${longitudeField.value}&current=temperature_2m,wind_speed_10m&hourly=temperature_2m,relative_humidity_2m,wind_speed_10m`, {
        mode : 'cors',
        method : "GET"
    });
    const res = await response.json();
    console.log(res);
    if (document.getElementById("div1"))
        section1.removeChild(div1);
    createCard(res);
    createRow(res);
    latitudeField.value = "";
    longitudeField.value = "";
});

