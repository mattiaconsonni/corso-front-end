const root = document.getElementById("root");
root.className = "background";

const header = document.createElement("header");
header.innerHTML =  `<nav class="navbar fixed-top navbar-expand-lg bg-body-tertiary bg-dark border-bottom border-body" data-bs-theme="dark">
<div class="container-fluid">
  <a class="navbar-brand" href="#">Save The Platypus</a>
  <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavDropdown">
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link active" id="home" aria-current="page" href="#">Home</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" id="storia" href="#">Storia</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" id="registrazione" href="#">Registrazione</a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
          Scopri i nostri ornitorinchi
        </a>
        <ul class="dropdown-menu">
          <li><a class="dropdown-item" href="#">Paolo</a></li>
          <li><a class="dropdown-item" href="#">Paolina</a></li>
          <li><a class="dropdown-item" href="#">Luca</a></li>
        </ul>
      </li>
    </ul>
  </div>
</div>
</nav>` 

// CARD 1

const section1 = document.createElement("section");
section1.className = "cards";
section1.id = "section1";
section1.style = "display: block"

const div1Card = document.createElement("div");
div1Card.className = "container text-center";
section1.appendChild(div1Card);

const div2Card = document.createElement("div");
div2Card.className = "row row-cols-3";
div1Card.appendChild(div2Card);

const div3Card = document.createElement("div");
div3Card.className = "col-12 col-md-4";
div2Card.appendChild(div3Card);


const div4Card = document.createElement("div");
div4Card.className = "card";
div4Card.style = "width: 18rem; height: 22rem;"
div3Card.appendChild(div4Card);

const image1 = document.createElement("img");
image1.src = "Images/Orn2.jfif";
image1.className = "card-img-top";
div4Card.appendChild(image1);


const div5Card = document.createElement("div");
div5Card.className = "card-body";
div4Card.appendChild(div5Card);


const p1 = document.createElement("p");
p1.className = "card-text";
p1.textContent = "Aiutate i nostri amici tuttofari... ormai sono in via di estinzione e solo TU potresti salvarli.";
div5Card.appendChild(p1);

// CARD 2

const div6Card = document.createElement("div");
div6Card.className = "col-12 col-md-4";
div2Card.appendChild(div6Card);

const div7Card = document.createElement("div");
div7Card.className = "card";
div7Card.style = "width: 18rem; height: 22rem;"
div6Card.appendChild(div7Card);

const div8Card = document.createElement("div");
div8Card.className = "card-header";
div7Card.appendChild(div8Card);

const b1 = document.createElement("b");
b1.textContent = "SVILUPPIAMOLI INSIEME";
b1.style = "font-family: monospace; color : red";
div8Card.appendChild(b1);

const div9Card = document.createElement("div");
div9Card.className = "card-body";
div7Card.appendChild(div9Card);

const bq = document.createElement("blockquote");
bq.className = "blockquote mb-0";
div9Card.appendChild(bq);

const p2 = document.createElement("p")
p2.textContent = "Take the platypus - that is not a finished product. It is clearly still in beta..";
bq.appendChild(p2);

const footer = document.createElement("footer");
footer.className = "blockquote-footer";
footer.textContent = "Stephen Colbert";
bq.appendChild(footer);

const cite = document.createElement("cite");
cite.textContent = "Late Show";
cite.title = "Source Title";
footer.append(document.createElement("br"));
footer.appendChild(cite);

// Card 3

const div10Card = document.createElement("div");
div6Card.className = "col-12 col-md-4";
div2Card.appendChild(div10Card);

const div11Card = document.createElement("div");
div11Card.className = "card";
div11Card.style = "width: 18rem; height: 22rem;"
div10Card.appendChild(div11Card);

const div12Card = document.createElement("div");
div12Card.className = "card-body";
div11Card.appendChild(div12Card);

const h5 = document.createElement("h5");
h5.className = "card-title";
h5.textContent = "Perché salvarli?";
div12Card.appendChild(h5);


const p3 = document.createElement("p");
p3.className = "card-text";
p3.textContent = "Questi splendidi animali, così ambivalenti hanno bisogno del nostro aiuto, noi ci impegniamo a salvarli creando una riserva per questi \"mammiferi\"";
div12Card.appendChild(document.createElement("br"));
div12Card.appendChild(p3);


const section2 = document.createElement("section");
section2.className = "container-car";
section2.id = "section2"
section2.innerHTML = `<div id="carouselExampleIndicators" class="carousel slide">
<div class="carousel-indicators">
  <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
  <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1" aria-label="Slide 2"></button>
  <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2" aria-label="Slide 3"></button>
</div>
<div class="carousel-inner">
  <div class="carousel-item active">
    <img src="Images/Orn3.jfif" class="d-block w-100" alt="ornitorinco1" height="900rem">
    <div class="carousel-caption d-none d-md-block">
        <h5>Sono sempre felici!</h5>
        <p>Non vorrai forse deluderli, vero?</p>
      </div>
  </div>
  <div class="carousel-item">
    <img src="Images/Orn4.jfif" class="d-block w-100" alt="ornitorinco gentleman" height="900rem" width="300px">
    <div class="carousel-caption d-none d-md-block">
        <h5>Sono animali di classe!</h5>
        <p>Dimostrati al loro livello e supporta la nostra fondazione!</p>
    </div>
  </div>
  <div class="carousel-item">
    <img src="Images/Orn6.jfif" class="d-block w-100" alt="ornitorinco cute" height="300rem" width="300px">
    <div class="carousel-caption d-none d-md-block">
        <h5>Non vedi come sono morbidi?</h5>
        <p>Viene a testare la loro morbidosità!</p>
    </div>
  </div>
</div>
<button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
  <span class="visually-hidden">Previous</span>
</button>
<button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
  <span class="carousel-control-next-icon" aria-hidden="true"></span>
  <span class="visually-hidden">Next</span>
</button>
</div>`

// FORM

const div13 = document.createElement("div");
div13.className = "container text-center";
div13.id = "div13";

const div14 = document.createElement("div");
div14.className = "row row-cols-1";
div13.appendChild(div14);

const div15 = document.createElement("div");
div15.className = "col";
div14.appendChild(div15);

const div16 = document.createElement("div");
div16.className = "respForm";
div15.appendChild(div16);

const div17 = document.createElement("div");
div16.appendChild(div17);

const h1 = document.createElement("h1");
h1.textContent = "Aiuta i nostri amici paperosi ORA!";
div17.appendChild(h1);

div16.appendChild(document.createElement("br"));

const form = document.createElement("form");
form.method = "POST";
form.action = "";
div16.appendChild(form);

const div18 = document.createElement("div");
div18.className = "mb-3";
div18.id = "labels"
form.appendChild(div18);

const label1 = document.createElement("label");
label1.for = "exampleFormControlInput1";
label1.class = "form-label";
label1.textContent = "Inserisci la tua mail";
div18.append(label1);


const input1 = document.createElement("input");
input1.type = "email";
input1.className = "form-control";
input1.id = "exampleFormControlInput1";
input1.placeholder = "ornito.rinco@amazonforest.com"
input1.required = true;
div18.appendChild(input1);

const div19 = document.createElement("div");
div18.className = "mb-3";
form.appendChild(div19);

const label2 = document.createElement("label");
label2.for = "exampleFormControlTextarea1";
label2.class = "form-label";
label2.textContent = "Scrivi perché vorresti salvarli!";
div19.append(label2);

const textarea = document.createElement("textarea");
textarea.className = "form-control";
textarea.id = "exampleFormControlTextarea1";
textarea.rows = 3;
div19.appendChild(textarea);

div19.appendChild(document.createElement("br"));

const select1 = document.createElement("select");
select1.className = "form-select form-select-sm";
div19.appendChild(select1);

const option1 = document.createElement("option");
option1.selected = true;
option1.disabled = true;
option1.textContent = "Quanto vorresti investire?";
select1.appendChild(option1);

const option2 = document.createElement("option");
option2.value = "1"
option2.textContent = "10";
select1.appendChild(option2);


const option3 = document.createElement("option");
option3.value = "2"
option3.textContent = "50";
select1.appendChild(option3);

const option4 = document.createElement("option");
option4.value = "3"
option4.textContent = "100";
select1.appendChild(option4);

const option5 = document.createElement("option");
option5.value = "5"
option5.textContent = "1000";
select1.appendChild(option5);

div19.appendChild(document.createElement("br"));

const input2 = document.createElement("input");
input2.type = "submit";
input2.style = "margin: auto"
input2.className = "btn btn-success";
input2.value = "Checkout"
div19.appendChild(input2);

const input3 = document.createElement("input");
input3.type = "reset";
input3.className = "btn btn-danger";
input3.value = "Cancella"
div19.appendChild(input3);

// Storia

const div20 = document.createElement("div");
div20.style = "margin-top : 5rem; background-color: white; padding: 10px; border: 2px solid red; border-radius: 30px"
div20.id = "div20"

const head = document.createElement("h1");
head.style = "font-family: monospace, color: red";
head.textContent = "Impara la storia degli ornitorinchi grazie al nostro partner National Geographic!";
div20.appendChild(head);

const frame = document.createElement("iframe");
frame.src = "https://www.nationalgeographic.com/animals/mammals/facts/platypus";
frame.style = "margin-top : 5rem; border: 2px solid red; margin-left: 10rem; margin-right: 10rem";
frame.width = "800rem";
frame.height = "500rem";
frame.id = "frame"

// Registrazione



const div21 = document.createElement("div");
div21.className = "container text-center";
div21.id = "div21"

const div22 = document.createElement("div");
div22.className = "row row-cols-1";
div21.appendChild(div22);


const div23 = document.createElement("div");
div23.className = "col";
div22.appendChild(div23);


const div24 = document.createElement("div");
div24.className = "respForm";
div23.appendChild(div24);


const div25 = document.createElement("div");
div24.appendChild(div25);


const regHead = document.createElement("h1");
regHead.textContent = "Registrati ora";
div25.appendChild(regHead);


div24.appendChild(document.createElement("br"));

const form2 = document.createElement("form");
form2.method = "POST";
form2.name = "specialForm";
form2.action = "";
div24.appendChild(form2);


const div26 = document.createElement("div");
div26.className = "mb-3";
div26.id = "labels2"
form2.appendChild(div26);


const label3 = document.createElement("label");
label3.for = "exampleFormControlInput2";
label3.class = "form-label";
label3.textContent = "Inserisci il tuo nome";
div26.append(label3);


const input4 = document.createElement("input");
input4.type = "text";
input4.className = "form-control";
input4.id = "exampleFormControlInput2";
input4.placeholder = "Nome"
input4.required = true;
div26.appendChild(input4);

div26.appendChild(document.createElement("br"));

const label4 = document.createElement("label");
label4.for = "exampleFormControlInput3";
label4.class = "form-label";
label4.textContent = "Inserisci il tuo cognome";
div26.append(label4);


const input5 = document.createElement("input");
input5.type = "text";
input5.className = "form-control";
input5.id = "exampleFormControlInput3";
input5.placeholder = "Cognome"
input5.required = true;
div26.appendChild(input5);

div26.appendChild(document.createElement("br"));

const label5 = document.createElement("label");
label5.for = "exampleFormControlInput4";
label5.class = "form-label";
label5.textContent = "Inserisci la tua mail";
div26.append(label5);


const input6 = document.createElement("input");
input6.type = "email";
input6.name = "mail";
input6.className = "form-control";
input6.id = "exampleFormControlInput4";
input6.placeholder = "E-mail"
input6.required = true;
div26.appendChild(input6);

div26.appendChild(document.createElement("br"));

const label6 = document.createElement("label");
label6.for = "exampleFormControlInput5";
label6.class = "form-label";
label6.textContent = "Inserisci la tua password";
div26.append(label6);



const input7 = document.createElement("input");
input7.type = "password";
input7.className = "form-control";
input7.name = "pass";
input7.id = "exampleFormControlInput5";
input7.placeholder = "Password"
input7.required = true;
div26.appendChild(input7);

div26.appendChild(document.createElement("br"));


const input8 = document.createElement("input");
input8.type = "submit";
input8.id = "btnSpecial"
input8.style = "margin: auto"
input8.className = "btn btn-success";
input8.value = "Registrati";
div26.appendChild(input8);

// Validazione mail e password


let mailField;
let passwordField;


root.appendChild(header);
root.appendChild(section1);
root.appendChild(section2);
root.appendChild(div13);

const storia = document.getElementById("storia");

const home = document.getElementById("home");

const registrazione = document.getElementById("registrazione");

storia.addEventListener('click', () => {
  storia.className = "nav-link active"
  home.className = "nav-link"
  registrazione.className = "nav-link"
  if (document.getElementById("div21"))
    root.removeChild(div21);
  if (document.getElementById("section1"))
    root.removeChild(section1);
  if (document.getElementById("section2"))
    root.removeChild(section2);
  if (document.getElementById("div13"))
    root.removeChild(div13);
  root.appendChild(div20);
  root.appendChild(frame);
})

home.addEventListener('click', () => {
  storia.className = "nav-link";
  home.className = "nav-link active";
  registrazione.className = "nav-link"
  if (document.getElementById("div20"))
    root.removeChild(div20);
  if (document.getElementById("frame"))
    root.removeChild(frame);
  if (document.getElementById("div21"))
    root.removeChild(div21);
  root.appendChild(section1);
  root.appendChild(section2);
  root.appendChild(div13);
})

registrazione.addEventListener ('click', () => {
  home.className = "nav-link";
  registrazione.className = "nav-link active";
  storia.className = "nav-link";
  if (document.getElementById("section1"))
    root.removeChild(section1);
  if (document.getElementById("section2"))
    root.removeChild(section2);
  if (document.getElementById("div13"))
    root.removeChild(div13);
  if (document.getElementById("div20"))
    root.removeChild(div20);
  if (document.getElementById("frame"))
    root.removeChild(frame);
  root.appendChild(div21);
  mailField = form2.mail;
  passwordField = form2.pass;
})


if (document.getElementById("btnSpecial")) {
  document.getElementById("btnSpecial").addEventListener('click', (e) => {
    e.preventDefault();
    console.log(passwordField.value.match(passValidation));
    console.log(passwordField);
    if (mailField.value.match(mailValidation)) {
        alert("Mail giusta!")
    }
    else {
        alert("Mail sbagliata!")
    }
    if (passwordField.value.match(passValidation)) {
        alert("Password giusta!")
    }
    else {
        alert("Password errata!")
    }
  })
}




