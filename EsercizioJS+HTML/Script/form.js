async function handleGetPosts() {
    const response = await fetch("https://jsonplaceholder.typicode.com/posts", {
        mode: 'cors',
        method : 'GET'
    });
    return await response.json();
}


const root = document.createElement("div");
document.getElementsByTagName("body")[0].appendChild(root);

function createCard(element) {
    const div1 = document.createElement("div");
    div1.className = "card";
    div1.style = "width: 18rem;"

    const div2 = document.createElement("div");
    div2.className = "card-body";
    div1.appendChild(div2);

    const p1 = document.createElement("p");
    p1.className = "card-text";
    p1.textContent = `${element.userId}`;
    div2.appendChild(p1);

    const p2 = document.createElement("p");
    p2.className = "card-text";
    p2.textContent = `${element.id}`;
    div2.appendChild(p2);

    const p3 = document.createElement("p");
    p3.className = "card-text";
    p3.textContent = `${element.title}`;
    div2.appendChild(p3);

    const p4 = document.createElement("p");
    p4.className = "card-text";
    p4.textContent = `${element.body}`;
    div2.appendChild(p4);

    root.appendChild(div1);
}

document.getElementById("btnSpecial").addEventListener('click', async () => {
    const response = await handleGetPosts();
    response.forEach(element => {
        createCard(element);
    });
});


