const specialForm = document.forms.specialForm;

const mailField = specialForm.mail;
const passwordField = specialForm.pass

const passValidation = /^.*(?=.{8,})(?=.*[a-zA-Z])(?=.*\d)(?=.*[!#$%&? "]).*$/g; 
const mailValidation = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/g;

document.getElementById("btnSpecial").addEventListener('click', (e) => {
    e.preventDefault();
    console.log(passwordField.value.match(passValidation));
    if (mailField.value.match(mailValidation)) {
        alert("Mail giusta!")
    }
    else {
        alert("Mail sbagliata!")
    }
    if (passwordField.value.match(passValidation)) {
        alert("Password giusta!")
    }
    else {
        alert("Password errata!")
    }
})