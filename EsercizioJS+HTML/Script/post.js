async function testPost(object) {
    const response = await fetch("https://jsonplaceholder.typicode.com/posts", {
        mode : 'cors',
        method : "POST",
        body : JSON.stringify(object)
    })

    const res = await response.json();
    return res;
}

const persona = {
    nome : "Simone",
    cognome : "De Meis"
}

const jsonString = JSON.stringify(persona);

document.getElementById("btnSpecial").addEventListener('click', async () => {
    await testPost(persona);
})

