const root = document.getElementById("root");

const buttonGrow = document.createElement("button");
buttonGrow.textContent = "Fallo crescere";
buttonGrow.className = "btn btn-success";

const imagePlatypyus = document.createElement("img");
imagePlatypyus.src = "Images/babyP.jfif";


const buttonBaby = document.createElement("button");
buttonBaby.textContent = "Benjamin Button";
buttonBaby.className = "btn btn-danger";

root.appendChild(buttonGrow);
root.appendChild(imagePlatypyus);
root.appendChild(buttonBaby);

buttonGrow.addEventListener("click", () => {
    imagePlatypyus.src = "Images/Adult.jfif";
})

buttonBaby.addEventListener("click", () => {
    imagePlatypyus.src = "Images/babyP.jfif";
})