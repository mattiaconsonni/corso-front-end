// Classe persona

class Persona {
    constructor(nome, cognome, eta) {
        this.nome = nome;
        this.cognome = cognome;
        this.eta = eta;
    }

    saluta() {
        console.log(`Ciao, ${this.nome} ${this.cognome}!`);
    }
}

const persona1 = new Persona("Mattia", "Consonni", 25);
persona1.saluta();

// Classe libro

class Libro {
    constructor(titolo, autore, annoPubblicazione) {
        this.titolo = titolo;
        this.autore = autore;
        this.annoPubblicazione = annoPubblicazione;
    }

    descrizione() {
        return `Il libro ${this.titolo} dell'autore ${this.autore} è stato pubblicato nel ${this.annoPubblicazione}.`;
    }

    static etaLibro(libro) {
        return new Date().getFullYear() - libro.annoPubblicazione;
    }
}

const libro1 = new Libro("La stagione delle Tempeste", "Sapkowski", 1984);
console.log(libro1.descrizione());

// Esercizio 3

console.log(Libro.etaLibro(libro1));

// Esercizio 4

class Studente extends Persona {
    constructor(nome, cognome, eta, corsoDiStudi) {
        super(nome, cognome, eta);
        this.corsoDiStudi = corsoDiStudi;
    }

    descrizione() {
        return `Lo studente ${this.nome} ${this.cognome} del corso di studi di ${this.corsoDiStudi}.`
    }
}

const persona2 = new Studente("Chiara", "Giudici", 26, "Intelligenza Artificiale");

console.log(persona2.descrizione());