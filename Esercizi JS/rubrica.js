// Creazione della classe contratto
class Contatto {
    constructor (nome, telefono, mail) {
        this.nome = nome;
        this.telefono = telefono;
        this.mail = mail;
    }
}

// Classe gestione rubrica

class GestioneRubrica {
    constructor() {
        this.listaContatti = [];
    }
    // Funzione che crea il contatto
    creaContatto(nome, telefono, mail) {
        return new Contatto(nome, telefono, mail);
    }

    // Funzione che agguinge un contatto
    aggiungiContatto(contatto) {
        console.log(`Contatto di nome ${contatto.nome} aggiunto!`);
        this.listaContatti.push(contatto);
    }

    // Funzione che stampa i contatti nella lista
    visualizzaRubrica() {
        this.listaContatti.forEach(elemento => console.log(`${elemento.nome} : ${elemento.telefono}, ${elemento.mail}`));
    }

    // Ricerca del contatto
    cercaContatto(nome) {
        for (let i = 0; i < this.listaContatti.length; i++) {
            if (this.listaContatti[i].nome === nome) {
                console.log(`Trovato: ${this.listaContatti[i].nome} : ${this.listaContatti[i].telefono}, ${this.listaContatti[i].mail}`);
                return this.listaContatti[i];
            }
        }
        console.log("Il contatto che cerchi non è stato trovato!");
        return null;
    }

    // Modifica del contatto
    modificaContatto(contatto, nuovoNome, nuovoTelefono, nuovaMail) {
        for (let i = 0; i < this.listaContatti.length; i++) {
            if (this.listaContatti[i].nome === contatto.nome) {
                this.listaContatti[i].nome = nuovoNome;
                this.listaContatti[i].telefono = nuovoTelefono;
                this.listaContatti[i].mail = nuovaMail; 
                console.log(`Modificato: ${this.listaContatti[i].nome} : ${this.listaContatti[i].telefono}, ${this.listaContatti[i].mail}`);
                return;
            }
        }
        console.log("Contatto non trovato!");
    }

    // Eliminazione contatto
    eliminaContatto(contatto) {
        let index = -1;
        for (let i = 0; i < this.listaContatti.length; i++) {
            if (this.listaContatti[i].nome === contatto.nome) {
                index = i;
                break;
            }
        }
        if (index === -1) {
            console.log("Contatto non trovato!");
        }
        else {
            console.log(`Eliminato: ${this.listaContatti[index].nome} : ${this.listaContatti[index].telefono}, ${this.listaContatti[index].mail}`);
            this.listaContatti.splice(index, 1);
        }
    }
}

// Testing

// Creazione rubrica
const rubrica = new GestioneRubrica();

// Creazione contatti
const contatto1 = rubrica.creaContatto("Mattia", 3661875499, "mattia.consonni@gmail.com");
const contatto2 = rubrica.creaContatto("Paolo", 3231874299, "paolo.primo@gmail.com");
const contatto3 = rubrica.creaContatto("Chiara", 3401244568, "chiara.giudici@gmail.com");
const contatto4 = rubrica.creaContatto("Luca", 3234567743, "luca.luchi@gmail.com");


// Aggiunta contatti
console.log("Aggiunta: ");
rubrica.aggiungiContatto(contatto1);
rubrica.aggiungiContatto(contatto2);
rubrica.aggiungiContatto(contatto3);
rubrica.aggiungiContatto(contatto4);

// Visualizzazione

console.log("Visualizza: ");
rubrica.visualizzaRubrica();

// Ricerca Contatto presente
console.log("Ricerca: ");

rubrica.cercaContatto("Paolo");

// Ricerca Contatto non presente

rubrica.cercaContatto("Vanessa");

// Modifica contatto esistente 

console.log("Modifica: ");

rubrica.modificaContatto(contatto2, "Piero", 345678298, "piero.pieri@gmail.com");
rubrica.visualizzaRubrica();

// Modifica contatto non in lista;

const contatto5 = new Contatto("Lu", 348876382, "lu@esempio.com");
rubrica.modificaContatto(contatto5, "Piero", 345678298, "piero.pieri@gmail.com");

// Eliminazione contatto corretto

console.log("Eliminazione: ");

rubrica.eliminaContatto(contatto4);
rubrica.visualizzaRubrica();

// Eliminazione contatto non esistente

rubrica.eliminaContatto("Paolo");





