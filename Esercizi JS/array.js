// Esercizio 1

const finalArray = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

function sommaValori(lista) {
    return finalArray.reduce((accumulator, currentValue) => accumulator + currentValue, 0);
}

console.log(sommaValori(finalArray));

// Esercizio 2

function getMax(lista) {
    return Math.max(...lista);
}

console.log(getMax(finalArray));

// Esercizio 3

function valoriPari(lista) {
    return lista.filter(element => element % 2 === 0)
}

console.log(valoriPari(finalArray));

// Esercizio 4

const a1 = [1, 2, 3];
const a2 = [100, 101, 102];

const a3 = [...a1, ...a2];

console.log(a3);

// Esercizio 5

const unsortedArray = [2, 7, 3, 9, 1, 4, 12];

function sortArray(lista) {
    return lista.slice().sort((a,b) => a-b);
}

console.log(sortArray(unsortedArray));

console.log(unsortedArray);

